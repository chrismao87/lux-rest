package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/chrismao87/lux-bot/public"
	"go.mongodb.org/mongo-driver/mongo"
)

type Server struct {
	binanceClient public.BotClient
	alpacaClient  public.BotClient
	router        *gin.Engine
	mongodb       *mongo.Database
}

func NewServer(binanceClient, alpacaClient public.BotClient, mongodb *mongo.Database) *Server {
	server := &Server{
		binanceClient: binanceClient,
		alpacaClient:  alpacaClient,
		mongodb:       mongodb,
	}
	router := gin.Default()
	// Allows all origins
	router.Use(cors.Default())

	url := ginSwagger.URL("/swagger/doc.json")
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	router.GET("/bots/grid", server.listGridBots)
	router.POST("/bots/grid", server.createGridBot)
	router.GET("/bots/grid/:id", server.getGridBot)
	router.PUT("/bots/grid/:id", server.updateGridBot)
	router.GET("/bots/grid/:id/summary", server.getGridBotSummary)

	server.router = router
	return server
}

func (server *Server) Start(address string) error {
	return server.router.Run(address)
}

func errorResponse(err error) gin.H {
	return gin.H{"error": err.Error()}
}
