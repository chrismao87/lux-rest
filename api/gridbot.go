package api

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/chrismao87/lux-bot/public"
	"gitlab.com/chrismao87/lux-rest/util"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

type getGridBotRequest struct {
	ID string `uri:"id" binding:"required"`
}

// @Summary Get GridBot
// @Description get gridbot by ID
// @Tags bots
// @Produce  json
// @Param id path string true "Bot ID"
// @Success 200 {string} Body
// @Router /bots/grid/{id} [get]
func (server *Server) getGridBot(ctx *gin.Context) {
	var req getGridBotRequest
	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	objId, err := primitive.ObjectIDFromHex(req.ID)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}
	c := server.mongodb.Collection(util.GridBotCollectionName)
	stmt := bson.M{
		"_id":  objId,
		"type": "GRID",
	}
	dbCtx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	res := c.FindOne(dbCtx, stmt)
	if res.Err() != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	var result bson.M
	if err := res.Decode(&result); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, result)
}

// @Summary List GridBots
// @Description list all gridbots
// @Tags bots
// @Produce  json
// @Success 200 {string} Body
// @Router /bots/grid [get]
func (server *Server) listGridBots(ctx *gin.Context) {
	c := server.mongodb.Collection(util.GridBotCollectionName)
	stmt := bson.M{
		"$and": []bson.M{
			{"type": "GRID"},
			{"$or": []bson.M{{"status": "STARTED"}, {"status": "STOPPED"}}},
		},
	}
	dbCtx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	cur, err := c.Find(dbCtx, stmt)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	defer cur.Close(dbCtx)

	list := bson.A{}
	for cur.Next(dbCtx) {
		var result bson.M
		if err := cur.Decode(&result); err != nil {
			ctx.JSON(http.StatusBadRequest, errorResponse(err))
			return
		}
		list = append(list, result)
	}
	ctx.JSON(http.StatusOK, list)
}

type createGridBotRequest struct {
	Symbol      string `json:"symbol" binding:"required"`
	Exchange    string `json:"exchange" binding:"required,oneof=binance alpaca"`
	UpperLimit  string `json:"upper_limit" binding:"required"`
	LowerLimit  string `json:"lower_limit" binding:"required"`
	GridQty     int32  `json:"grid_qty" binding:"required,min=1,max=100"`
	UnitPerGrid string `json:"unit_per_grid" binding:"required"`
}

type createGridBotResponse struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

// @Summary Create GridBot
// @Description create a gridbot
// @Tags bots
// @Accept  json
// @Produce  json
// @Param createGridBotRequest body api.createGridBotRequest true "Create bot"
// @Success 200 {object} api.createGridBotResponse
// @Router /bots/grid [post]
func (server *Server) createGridBot(ctx *gin.Context) {
	var req createGridBotRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	createReq := &public.CreateBotRequest{BotOption: &public.CreateBotRequest_GridOpt{
		GridOpt: &public.GridBotOpt{
			Symbol:      req.Symbol,
			UpperLimit:  req.UpperLimit,
			LowerLimit:  req.LowerLimit,
			GridQty:     req.GridQty,
			UnitPerGrid: req.UnitPerGrid,
		},
	}}
	var rpcResp *public.CreateBotResponse
	var rpcError error
	var resp *createGridBotResponse
	switch req.Exchange {
	case "binance":
		rpcResp, rpcError = server.binanceClient.CreateBot(ctx, createReq)
	case "alpaca":
		rpcResp, rpcError = server.alpacaClient.CreateBot(ctx, createReq)
	default:
		ctx.JSON(http.StatusBadRequest, errorResponse(fmt.Errorf("unsupported exchange: %s", req.Exchange)))
		return
	}
	if rpcError != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(rpcError))
		return
	}
	resp = &createGridBotResponse{
		ID:   rpcResp.GetId().GetId(),
		Type: rpcResp.GetId().GetType().String(),
	}
	ctx.JSON(http.StatusOK, resp)
	return
}

type updateGridBotRequest struct {
	Status string `json:"status" binding:"required,oneof=STARTED STOPPED DELETED"`
}

// @Summary Update GridBot
// @Description update a gridbot
// @Tags bots
// @Accept  json
// @Produce  json
// @Param id path string true "Bot ID"
// @Param updateGridBotRequest body api.updateGridBotRequest true "Update bot"
// @Success 200
// @Router /bots/grid/{id} [put]
func (server *Server) updateGridBot(ctx *gin.Context) {
	var req updateGridBotRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	id := ctx.Param("id")
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}
	c := server.mongodb.Collection(util.GridBotCollectionName)
	stmt := bson.M{
		"_id":  objId,
		"type": "GRID",
	}
	dbCtx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	res := c.FindOne(dbCtx, stmt)
	if res.Err() != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	var result bson.M
	if err := res.Decode(&result); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	exchange := result["exchange"]
	var botClient public.BotClient
	switch exchange {
	case "binance":
		botClient = server.binanceClient
	case "alpaca":
		botClient = server.alpacaClient
	default:
		ctx.JSON(http.StatusBadRequest, errorResponse(fmt.Errorf("unsupported exchange: %s", exchange)))
		return
	}
	switch req.Status {
	case "STARTED":
		_, err := botClient.StartBot(ctx, &public.StartBotRequest{Id: &public.BotId{
			Id:   id,
			Type: public.BotType_GRID,
		}})
		if err != nil {
			ctx.JSON(http.StatusBadRequest, errorResponse(err))
			return
		}
	case "STOPPED":
		_, err := botClient.StopBot(ctx, &public.StopBotRequest{Id: &public.BotId{
			Id:   id,
			Type: public.BotType_GRID,
		}})
		if err != nil {
			ctx.JSON(http.StatusBadRequest, errorResponse(err))
			return
		}
	case "DELETED":
		_, err := botClient.DeleteBot(ctx, &public.DeleteBotRequest{Id: &public.BotId{
			Id:   id,
			Type: public.BotType_GRID,
		}})
		if err != nil {
			ctx.JSON(http.StatusBadRequest, errorResponse(err))
			return
		}
	}
	ctx.JSON(http.StatusOK, "")
	return
}

type getGridBotSummaryRequestUri struct {
	ID string `uri:"id"  binding:"required"`
}

type getGridBotSummaryRequestForm struct {
	Exchange string `form:"exchange" binding:"required,oneof=binance alpaca"`
}

// @Summary Get GridBot Summary
// @Description get gridbot summary
// @Tags bots
// @Produce  json
// @Param id path string true "Bot ID"
// @Param exchange query string true "Exchange"
// @Success 200 {string} Body
// @Router /bots/grid/{id}/summary [get]
func (server *Server) getGridBotSummary(ctx *gin.Context) {
	var uri getGridBotSummaryRequestUri
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	var form getGridBotSummaryRequestForm
	if err := ctx.ShouldBind(&form); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	var botClient public.BotClient
	switch form.Exchange {
	case "binance":
		botClient = server.binanceClient
	case "alpaca":
		botClient = server.alpacaClient
	default:
		ctx.JSON(http.StatusBadRequest, errorResponse(fmt.Errorf("unsupported exchange: %s", form.Exchange)))
		return
	}
	rpcResp, err := botClient.GetBotStat(ctx, &public.GetBotStatRequest{
		Id: &public.BotId{
			Id:   uri.ID,
			Type: public.BotType_GRID,
		},
	})
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rpcResp.GetGridBotSummary())
}
