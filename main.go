package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/chrismao87/lux-bot/public"
	"gitlab.com/chrismao87/lux-rest/api"
	_ "gitlab.com/chrismao87/lux-rest/docs" // docs is generated by Swag CLI, you have to import it.
	"gitlab.com/chrismao87/lux-rest/util"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"time"
)

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @termsOfService http://swagger.io/terms/
func main() {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.WithError(err).Fatal("failed to load config")
	}

	mongoClient, err := mongo.Connect(context.Background(), options.Client().ApplyURI(config.MongoConnectURL))
	if err != nil {
		log.WithError(err).Fatal("failed to connect mongo")
	}

	db := mongoClient.Database("lux")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	binanceExchangeConn, err := grpc.DialContext(ctx, config.BinanceBotGrpcAddr, grpc.WithInsecure(), grpc.WithBlock())
	defer binanceExchangeConn.Close()
	if err != nil {
		log.WithError(err).Fatalf("did not connect: %v", err)
	}
	alpacaExchangeConn, err := grpc.DialContext(ctx, config.AlpacaBotGrpcAddr, grpc.WithInsecure(), grpc.WithBlock())
	defer alpacaExchangeConn.Close()
	if err != nil {
		log.WithError(err).Fatalf("did not connect: %v", err)
	}
	server := api.NewServer(public.NewBotClient(binanceExchangeConn), public.NewBotClient(alpacaExchangeConn), db)

	err = server.Start(config.ListenAddress)
	if err != nil {
		log.WithError(err).Fatal("failed to start server")
	}
}
