package util

import "github.com/spf13/viper"

const EnvPrefix = "LUX"

type Config struct {
	BinanceBotGrpcAddr string `mapstructure:"BINANCE_BOT_GRPC_ADDR"`
	AlpacaBotGrpcAddr  string `mapstructure:"ALPACA_BOT_GRPC_ADDR"`
	MongoConnectURL    string `mapstructure:"MONGO_CONNECT_URL"`
	ListenAddress      string `mapstructure:"LISTEN_ADDRESS"`
}

func LoadConfig(path string) (config Config, err error) {
	viper.SetEnvPrefix(EnvPrefix)
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}
	err = viper.Unmarshal(&config)
	return
}
